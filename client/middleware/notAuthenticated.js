import jwtDecode from 'jwt-decode'
export default function ({ app, redirect }) {
  const token = app.$cookies.get('token')
  if (token && jwtDecode(token).exp > Date.now() / 1000) {
    return redirect('/')
  }
}
