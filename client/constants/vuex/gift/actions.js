// For commit mutations in the same module
// Use { root: true } as the third argument
export default {
  GETRANDOMGIFT: 'gift/getRandomGift',
}
