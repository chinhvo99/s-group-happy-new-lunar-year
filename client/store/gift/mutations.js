import Vue from 'vue'
export default {
  SET_GIFT(state, payload) {
    Vue.set(state, 'gift', payload)
  },
  SET_WISH(state, payload) {
    Vue.set(state, 'wish', payload)
  },
}
