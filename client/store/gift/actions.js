export default {
  async getRandomGift({ commit }) {
    try {
      const response = await this.$axios.post(
        '/v1/gifts/random-my-gift',
        {},
        {
          headers: { authorization: 'Bearer ' + this.$cookies.get('token') },
        }
      )
      commit('SET_GIFT', response.data.gift)
      commit('SET_WISH', response.data.wish)
    } catch (e) {
      throw e.response
    }
  },
  // logout({ commit }) {
  //   localStorage.removeItem('auth')
  //   commit(authMutations.SET.AUTH, null, { root: true })
  // },
}
