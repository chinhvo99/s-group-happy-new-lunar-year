export default {
  // login({ commit }, form) {
  //   let auth = null
  //   // Faking auth data
  //   auth = {
  //     ...form,
  //     role: 'ADMIN',
  //     accessToken: 'yourAccessTokenFromBackend',
  //   }
  //   localStorage.setItem('auth', JSON.stringify(auth))
  //   commit(authMutations.SET.AUTH, auth, { root: true }) // Mutating to store for client rendering
  // },
  async login({ commit }, payload) {
    const auth = {
      userID: payload.userID,
      accessToken: payload.accessToken,
    }
    try {
      const response = await this.$clientApi.post('/v1/auth/login-fb', auth)
      this.$cookies.set('token', response.data.data.token)
    } catch (e) {
      throw e.response.data.message[0].description
    }
  },
  // logout({ commit }) {
  //   localStorage.removeItem('auth')
  //   commit(authMutations.SET.AUTH, null, { root: true })
  // },
}
